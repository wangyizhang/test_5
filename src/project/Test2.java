package project;

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;

public class Test2 {

    public static void main(String [] args) throws Exception{

       // new Thread(() -> {System.out.println("hello !");}).start();

//        Runnable t =() ->{ System.out.println("hello");};
//        new Thread(t).start();
//
//        Callable<String> call =new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return null;
//            }
//        };
//
//        Callable<String> call2 = () -> {return "wang";};
//
//        System.out.println(call2.call());

//        UserMapper m = (User user) -> {return user.hashCode();};
//        System.out.println(m.getName(new User()));
        UserMapper m2 =(user) -> get();
        System.out.println(m2.getName("wang"));



        Function<Integer,Integer> f = (a) -> {
            int sum =0;
            for(int i=1;i<=a;i++){
                sum+= i;
            }
            return sum;
        };
        System.out.println(f.apply(10));
    }

    public static  int get(){
        return 2;
    };

    Consumer<Integer > cu = ( a) -> {
        System.out.println(1);
    };

}

interface UserMapper{
    int getName(String user);
}

class User{

}
