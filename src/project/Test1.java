package project;

import java.lang.invoke.LambdaMetafactory;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;

public class Test1 {
    public static void main(String [] args){

        Test1 test =new Test1();
        int s =1;
        // Operation1 exp1 =(int a,int b)->(a+b);
        Operation1 exp2 =(a,b) -> a-b;
        Operation1 exp3 =(a,b) -> a/b;

        System.out.println("10+5 = "+test.operate(10,5,(a,b)->a+b));
        System.out.println("10-5 = "+test.operate(10,5,exp2));
        System.out.println("10/5 = "+test.operate(10,5,exp3));

        Operation2 op1 =(int a,int b)->System.out.println(a+b);
        Operation2 op2 =(a,b)->{
            System.out.println(a-b);
        };
        op1.printNum(10,6);
        op2.printNum(10,6);


    }

    public int operate(int a,int b,Operation1 target){
        return target.getNum(a,b);
    }

}

@FunctionalInterface
interface Operation1{
    int getNum(int a,int b);

}

@FunctionalInterface
interface Operation2{
    void printNum(int a,int b);
}

